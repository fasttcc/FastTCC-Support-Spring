package com.bosssoft.platform.fasttcc.support.spring;

import com.bosssoft.platform.fasttcc.support.BeanClassFinder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringBeanClassFinder implements BeanClassFinder, ApplicationContextAware
{
    private ApplicationContext applicationContext;

    @Override
    public Class<?> find(String beanName)
    {
        Object bean = applicationContext.getBean(beanName);
        return bean.getClass();
    }

    public ApplicationContext getApplicationContext()
    {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext)
    {
        this.applicationContext = applicationContext;
    }
}
