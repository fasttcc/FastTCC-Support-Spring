package com.bosssoft.platform.fasttcc.support.spring;

import com.bosssoft.platform.fasttcc.support.CompleteStageHelper;
import com.jfireframework.baseutil.reflect.ReflectUtil;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.lang.reflect.Method;

public class SpringCompleteStageHelper implements CompleteStageHelper, ApplicationContextAware
{
    private ApplicationContext applicationContext;

    @Override
    public void invokeMethod(Method method, Class<?> ckass, Object[] params)
    {
        Object bean = applicationContext.getBean(ckass);
        try
        {
            method.invoke(bean, params);
        }
        catch (Throwable e)
        {
            ReflectUtil.throwException(e);
        }
    }

    public ApplicationContext getApplicationContext()
    {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext)
    {
        this.applicationContext = applicationContext;
    }
}
