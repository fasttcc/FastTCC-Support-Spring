package com.bosssoft.platform.fasttcc.support.spring;

import com.bosssoft.platform.fasttcc.support.JdbcTransactionInfoDetect;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;

public class SpringJdbcTransactionInfoDetect implements JdbcTransactionInfoDetect
{

    @Override
    public TransactionBehavior detect(Method method)
    {
        Transactional transactional = method.getAnnotation(Transactional.class) == null ? method.getDeclaringClass().getAnnotation(Transactional.class) : method.getAnnotation(Transactional.class);
        if (transactional == null)
        {
            throw new IllegalArgumentException("方法" + method.toGenericString() + "无法找到事务信息");
        }
        Propagation propagation = transactional.propagation();
        switch (propagation)
        {
            case REQUIRED:
                return TransactionBehavior.PROPAGATION_REQUIRED;
            case SUPPORTS:
                return TransactionBehavior.PROPAGATION_SUPPORTS;
            case MANDATORY:
                return TransactionBehavior.PROPAGATION_MANDATORY;
            case REQUIRES_NEW:
                return TransactionBehavior.PROPAGATION_REQUIRES_NEW;
            case NOT_SUPPORTED:
                return TransactionBehavior.PROPAGATION_NOT_SUPPORTED;
            case NEVER:
                return TransactionBehavior.PROPAGATION_NEVER;
            case NESTED:
                return TransactionBehavior.PROPAGATION_NEVER;
            default:
                throw new NullPointerException();
        }
    }
}
